package nugraha.aditya.uts_126_143

import android.app.AlertDialog
import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.widget.LinearLayout
import android.widget.Toast
import cn.pedant.SweetAlert.SweetAlertDialog
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.messaging.FirebaseMessaging
import com.google.gson.Gson
import kotlinx.android.synthetic.main.activity_menu.*
import kotlinx.android.synthetic.main.activity_pesan.view.*
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch

class MenuActivity : AppCompatActivity(), View.OnClickListener {
    var fbaut= FirebaseAuth.getInstance()
    lateinit var viewDial : View
    val Tag = "openfcm"
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_menu)
        menu1.setOnClickListener(this)
        menu3.setOnClickListener(this)
        logout.setOnClickListener(this)
    }

    override fun onClick(v: View?) {
        when(v?.id){
            R.id.menu1->{
                val intent = Intent(this, MainActivity::class.java)
                startActivity(intent)
            }
            R.id.menu3->{
                val intent = Intent(this, MapActivity::class.java)
                startActivity(intent)
            }
            R.id.logout->{
                logout1("Ingin Keluar Dari Aplikasi !!!")
            }
        }
    }


    fun logout1(string: String?) {
        SweetAlertDialog(this, SweetAlertDialog.WARNING_TYPE)
            .setTitleText("Warning")
            .setContentText(string)
            .setConfirmText("Iya")
            .setCancelText("Tidak")
            .setConfirmClickListener { sDialog -> sDialog.dismissWithAnimation()
                fbaut.signOut()
                finish()
            }
            .setCancelClickListener{ sDialog -> sDialog.dismissWithAnimation()
            }
            .show()
    }
}
